Installation Instructions


https://www.polymer-project.org/2.0/start/install-2-0



<br/>
1. Install Bower.

<br/><code>npm install -g bower</code>



2. Install the Polymer CLI.

<br/><code>npm install -g polymer-cli</code>



3. Download Dependencies

<br/><code>bower install</code>



4. Start Server

<br/><code>polymer serve</code>



5. Access using the port number 8081

<br/><code>http://localhost:8081/</code>



